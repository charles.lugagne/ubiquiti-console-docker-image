# Ubiquiti Console Docker Image

Choose a self-explaining name for your project.

## Description
Just a simple Dockerfile for personal use that I'm making public in case someone else finds it useful.
I am not affiliated in any way with Ubiquiti or anyone working there, it's just a personal project.

## Installation
Up to you, it's just a Dockerfile.

## Support
I don't offer support for this project, but feel free to open an issue if you find a bug or have a feature request.

## Contributing
Open for external contributions.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
This is integrating a proprietary software made by Ubiquiti.
I don't own any rights on it, but this image is free to use for anyone.

## Project status
Weekend project, not maintained.
